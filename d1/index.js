console.log(`Hello World!`)

// SECTION - JSON Objects
/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programmin language, hence the "Notation" in its name
	- JSON is not to be confuse with Javascript objects since aside from format itself, JS objects are excluive to javascript while JSON can be used by other languages as well
	SYNTAX:
		{
			"propertyA": "valueA",
			"propertyB": "valueB",
		}
*/

// Miniactivity
// below is an example of JS Object
/*let cities = [
	{city: "Pasig", province: "Manila", country: "Philippines"},
	{city: "Mandaluyong", province: "Manila", country: "Philippines"},
	{city: "Quezon", province: "Manila", country: "Philippines"},
];
*/

/*
	below is an example of JSON
		when logged in the console, it seems that he appearance of JSON does not differ from JS Obeject. but since JSON is used by other languages as well, it is common to see it as JS object since it is the blueprint of the JSON format
*/
/*
	WHAT JSON DOES
		- JSON is used for seializing different data types into bytes
		- bytes is a unit of data that is composed of 8 binary digits (1/0) that is used to represent a character (letters, numbers, typographical symbol)
		- serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
*/


let cities = [
	{"city": "Pasig", "province": "Manila", "country": "Philippines"},
	{"city": "Mandaluyong", "province": "Manila", "country": "Philippines"},
	{"city": "Quezon", "province": "Manila", "country": "Philippines"},
];

console.log(cities)


// JSON Methods
	// JSON object contains methods for parsing and converting data into/from JSON or stringified JSON

// SECTION - Stringify Method
	/*
		- stringified JSON is a JS object converted into a string (but in JSON format) to be used in other functions of a JS application
		- they are commonly used in HTTP requests where information is required to be send and received in a stringified JSON format
		- requests are an imporatnt part of programmin where an application communicates with a backedn application to perform different tasks such getting/creating data in a database
		- a front application is an application that used to interact with users to perform different tasks and display information while the backend application are commonly used for all business logic and data processing
		- a database normally stores information/data that can be used in most applications
		- commonly stored data in databases are user information, transaction records, and product information
		- Node/Express JS are two of technologies that are used for creating backend applications which process requests from frontend application
			- Node JS is a Java Runtime Environment (JRE)/soft ware that is made to execute other software
			- Express JS is a Node JS framwork that provides fetures for easily creating web and mobile applications
	*/
let batchesArray = [ {batchName: "Batch X"}, {batchName: "Batch Y"}];
console.log(batchesArray);
console.log(`Result from stringify method`);
console.log(JSON.stringify(batchesArray));

let data = {
	name: "Christopher",
	age: 25,
	address: {
		city: "Pasig",
		country: "Philippines"
	}
}
console.log(data)
console.log(JSON.stringify(data));

// direct conversion of data type from JS object into stringified JSON
data = JSON.stringify({
	name: "John",
	age: 25,
	address:{
		city: "Batangas",
		country: "Philippines",
	}
});

console.log(data);

// an example where JSON is commonly used in on package.json files which an express JS application uses to keep track of the information regarding a repository/priject (see package.json)

// JSON.stringify with prompt()
/*
	- When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable
	- the "property" name and the "value" name would be have to be the same and this might be a little confusing, but this is to signify that the value of the variable and the
 */
let fname = prompt("What is your first name?");
let lname = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which city do you live?"),
	country: prompt("Which country does your city belong?"),
}

let otherData = JSON.stringify({
	fname: fname,
	lname: lname,
	age: age,
	address: address,
})

console.log(otherData);

// parse method
/*
	- converts the stringified JSON into Javascript Objects
	- Objects are common data types used in applications because of the complex data structures that can be created out of them
	- information is commonly sent to applications in stringified JSON and the nconverted back into objects
	- this happens for both
*/


let batchesJSON = `[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]`;
console.log(batchesJSON);
console.log(`Result from parse method`);
console.log(JSON.parse(batchesJSON));

console.log(JSON.parse(data));
console.log(JSON.parse(otherData));